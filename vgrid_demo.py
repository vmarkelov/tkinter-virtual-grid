# -*- coding: UTF-8 -*-
#vgrid demo for python.3x

# Author Vladimir Markelov <vmatroskin@gmail.com>
# demo texts and styles are taken from the
# demo for the original grid by:
##### Grid widget class for Python2.x/Tkinter, Lisence: GPL
##### Author: C. Angelidis <cagge@teemail.gr>


from tkinter import *
import vgrid

def onRowSelect(event):
    n = event.x
    t = 'selected row = {0}'.format(n)
    st.configure(text=t)

def onRowEnter(event):
    n = event.x
    t = 'Open detailed view of row = {0}'.format(n)
    st.configure(text=t)

def load_data():
    Mygrid.setGridData(general_1, headers_1, grid_data_1)
    Mygrid.bind("<<RowChange>>", onRowSelect)
    Mygrid.bind("<<RowEnter>>", onRowEnter)
    st.configure(text='')
    Mygrid.setFocus()

def cellValue(row, col):
    if col == 0:
        return 'Product {0}'.format(row+1)
    elif col == 1:
        return 'Factory {0}'.format(row % 127 + 1)
    elif col == 2:
        return '{0}'.format(row % 1000 + row // 10000)
    else:
        return 'Item at ({0}, {1})'.format(row, col)

def loadManyData():
    Mygrid.setGridData(general_3, headers_3, None, cellValue)
    Mygrid.bind("<<RowChange>>", onRowSelect)
    st.configure(text='10,000 rows loaded')
    Mygrid.setFocus()

def getNumberOfRows():
    rows=Mygrid.getNumberOfRows()
    text='number of rows=  '+ str(rows)
    st.configure(text=text)

def selectRow():
    Mygrid.ensureVisible(4)
    st.configure(text='Row  4  selected')

def clearGrid():
    Mygrid.clearGrid()
    st.configure(text='grid is empty')


root = Tk()
root.title('vgrid demo')
root.geometry("+0+0")

# grid
Mygrid = vgrid.VirtualGrid(root, height=340, width=660, background='white')
Mygrid.grid(row=0, column=0, columnspan=6, rowspan=2, sticky=W, padx=20, pady=20)

# buttons
b1 = Button(root, text="Close", command=root.quit, height=1, bg='#ACACAC')
b1.grid(row=3, column=5, sticky=SW)

b2 = Button(root, text="Load data", command=load_data, width=15, height=1, bg='#ACACAC')
b2.grid(row=2, column=1, columnspan=1, sticky=W, pady=10)

b4 = Button(root, text="Load many data", command=loadManyData, width=15,height=1, bg='#ACACAC')
b4.grid(row=2, column=2, columnspan=1, sticky=W, pady=10)

b7 = Button(root, text="Get number of rows", command=getNumberOfRows, width=15, height=1, bg='#ACACAC')
b7.grid(row=2, column=4, columnspan=1, sticky=W, pady=10)

b11 = Button(root, text="Select row [4]", command=selectRow, width=15, height=1, bg='#ACACAC')
b11.grid(row=2, column=3, columnspan=1, sticky=W)

b21 = Button(root, text="Clear grid", command=clearGrid, width=15, height=1, bg='#ACACAC')
b21.grid(row=3, column=1, columnspan=1, sticky=W)

#labels
st = Label(root, font='Arial 10 bold', bg='light grey',bd=1, relief=SUNKEN, wraplength=850)
st.grid(row=5, column=0, columnspan=120, sticky=E+W, padx=10, pady=10)

st.configure(text='Click on button: Load data', font='Arial 12 bold')

general_1 = {
    'TABLE_TITLE': '',                  #0
    'TABLE_TITLE FONT': 'Serif 16 bold',#1
    'HEADERS BACKCOLOR': 'yellow',  #2
    'HEADERS FONT': 'Arial 12 bold',    #3
    'HEADERS ROWHEIGHT': 40,            #4
    'GRID_CELLS FONT': 'Arial 10',      #5
    'GRID CELLS ARE EDITABLE': True     #6
    }

headers_1 = [
    ['Product', 	200, 'left'],
    ['Description', 350, 'left'],
    ['Facrory', 	80, 'center'],
    ['Price', 		60, 'right']
]

grid_data_1 = [
    ['SALVA E CAMBIA CERAMICA', 'For the enamel and renovation of bathtubs, showers, basins, ceramic wall tiles, sanitary ware, elevtric stoves and refrigerators, kitchen sinks','ROS', '25.00'],
    ['MULTIFONDO', 				'New anti-rust paint used directly on the rust. Provides a finished surface', 'ROS', '11.50'],
    ['ANTIQUE' ,  				'Anti-rust enamel based on synthetic mastic and ferrum oxide', 'ROS', '11.00'],
    ['MITOS', 					'Fungicide. Exterminates woodworm', 'VEL', '2.50'],
    ['WAX STICKS', 				'Repairs holes and large cracks on damaged wood', 'VEL', '2.20'],
    ['OLD STYLE', 				'Keeps away the dust and Polishes. Based on gum lac', 'VEL', '7.00'],
    ['CERADO WAX', 				'Cleans, polishes and protects finished wooden surfaces', 'VEL', '4.30'],
    ['CERALEGNO', 				'Bee honey paste. Protects and polishes wood', 'VEL', '6.80'],
    ['RESTAURATORE CERSO', 		'Two in one. Liquid wax - Fungicide. Used to polish and color wood' , 'VEL' , '4.90'],
    ['DECERANTE', 				'Wood detergent', 'VEL', '5.20'],
    ['LEGNO LIBERO', 			'Cleans up and refreshes blackened wood', 'VEL', '9.70'],
    ['BIONDA', 					'Gum lac solution', 'VEL', '5.60'],
    ['LACCANTICA', 				'Undercoat. Pore sealer', 'VEL', '5.75'],
    ['LUNICA', 					'Liquid wax for parquets', 'VEL', '9.50'],
    ['WOOD MARKER', 			'Special marker used for the retouch of furniture. 12 colors', 'VEL', '2.20'],
    ['RITOCCO LEGNO', 			'For the retouch of the surface of the wood. Colors: Silver, Gold, Aged gold', 'UV', '4.80'],
    ['JOINT SEALER' , 			'Synthetic color for the coverage of joints of ceramic tablets through point pencil', 'UV', '2.00'],
    ['FIXING ENAMEL' , 			'Enamel appropriate for the fixing of porcelain objects', 'UV', '2.00'],
    ['SPLOCH CLEANER' , 		'Takes off a sploch made by rust, ink, fruit, beverages on marbles, mosaic floors, cloths', 'UV', '2.50'],
    ['VASELINE OIL', 			'For the lubrication of all household appliances such as door locks, door and window hinges, sewing machines', 'UV', '1.25'],
    ['LUBRICATING GREASE', 		'For all machine points that need constant lubrication such as door locks, chains, jacks, window devices', 'UV', '1.60'],
    ['VINYL GLUE', 				'Liquid glue appropriate for wood, plastics, melamine', 'UV', '1.00'],
    ['TRANSPARENT GLUE', 		'For paper, plastic and small pieces', 'UV', '1.00'],
    ['SPLENDOR', 				'Burnishes objects made of Gold, silver, copper and brass', 'UV', '1.40'],
    ['CABLE MM 3-5', 			'P.V.C-covered brass-coated steel cable', 'FACO', '2.10'],
    ['WIRE 15-01', 				'Multi-purpose hot dipped iron wire', 'FACO', '4.30']
]

#many grid data
general_3 = {
    'TABLE_TITLE': 'Demo of cgrid widget for Python/Tkinter',
    'TABLE_TITLE FONT': 'Serif 14 bold',
    'HEADERS BACKCOLOR':  'yellow', 	#<item>, itemvalue
    'HEADERS FONT': 'Arial 12 bold',
    'HEADERS ROWHEIGHT': 40,			#pixels
    'GRID_CELLS FONT': 'Arial 10',
    'GRID CELLS ARE EDITABLE': True,
    'ROW COUNT': 10000,
}

headers_3 = [
    ['Product', 280, 'left'],
    ['Factory', 80, 'center'],
    ['Price', 80, 'right']
]


root.mainloop()
