# -*- coding: UTF-8 -*-

#Virtual Grid widget class for Python3.x/Tkinter
#Author: Vladimir Markelov <vmatroskin@gmail.com>

from tkinter import *
import tkinter.ttk as ttk
import os, sys
import platform

__version__ = '0.1.0'
__author__  = 'Vladimir Markelov <vmatroskin@gmail.com>'

class VirtualGrid(ttk.Frame):
    def __init__(self, parent, height=None, width=None, background=None):
        ttk.Frame.__init__(self, parent)

        self.rootWidget = parent
        while self.rootWidget.master:
            self.rootWidget = self.rootWidget.master

        self.height     = 100
        self.width      = 100
        self.hscroll    = False
        self.vscroll    = False
        self.background = 'white'
        if height:
            self.height      = height
        if width:
            self.width      = width
        if background:
            self.background = background
        self.root_height = self.height
        self.root_width = self.width

        self.scrollAfter     = None
        self.rowHeaderWidth  = 50
        self.colHeaderHeight = 28
        self.position = -1
        self.topRow   = 0
        self.lastEventRow = -1

        self.columnconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)

        #column headers
        self.canvas1 = Canvas(self, width=self.width - self.colHeaderHeight, height=self.colHeaderHeight,
            background=self.background, bd=1, relief=RAISED)
        self.canvas1.grid(row=1, column=1, sticky=E+W)

        #row headers
        self.canvas2 = Canvas(self, width=self.rowHeaderWidth, height=self.height - self.colHeaderHeight,
            background=self.background, bd=1, relief=RAISED)
        self.canvas2.grid(row=2, column=0, sticky=N+S)

        #corner
        self.canvas3 = Canvas(self, background=self.background,
            bd=1, relief=RAISED, width=self.rowHeaderWidth, height=self.colHeaderHeight)
        self.canvas3.grid(row=1, column=0, sticky=N+S)

        #grid
        self.canvas = Canvas(self, width=self.width - self.colHeaderHeight, height=self.height - self.colHeaderHeight,
            bd=1, relief=RAISED, background=self.background)
        self.canvas.grid(row=2, column=1, sticky=E+W+N+S)

        self.lb = ttk.Label(self, background='#F2F1F0')
        self.clearGrid()
        self.setGridData()

    def setGridData(self, general=None, headers=None, grid_data=None, dataCallback=None):
        if general:
            self.general = general
        else:
            self.general = {
                'TABLE_TITLE': '',
                'TABLE_TITLE FONT': 'Serif 12 bold',
                'HEADERS BACKCOLOR': 'light cyan',
                'HEADERS FONT': 'Arial 12 bold',
                'HEADERS ROWHEIGHT': 28,
                'GRID_CELLS FONT': 'Arial 10',
                'ROW COUNT': 0,
                }
        if grid_data:
            self.general['ROW COUNT'] = len(grid_data)
        if headers:
            self.headers = headers
        else:
            self.headers = [['', self.width - self.rowHeaderWidth, 'left']]
        if grid_data:
            self.grid_data = grid_data
        else:
            self.grid_data = []
        self.dataCallback = dataCallback
        self.setData()

    def setData(self):
        self.canvas.delete(ALL)
        self.canvas2.delete(ALL)
        self.position = 0
        self.topRow   = 0
        self.position = -1
        self.topRow   = 0
        if self.hscroll:
            self.xscrollbar.grid_forget()
        if self.vscroll:
            self.yscrollbar.grid_forget()
        self.height = self.root_height
        self.width  = self.root_width


        self.numOfCols = len(self.headers)
        self.font = self.general['GRID_CELLS FONT']
        self.totalwidth = 0
        for i in range(self.numOfCols):
            self.totalwidth += self.headers[i][1]

        self.hscroll = True
        if self.totalwidth < self.width + 10:
            self.width = self.totalwidth
            self.hscroll = False
        if self.hscroll:
            self.xscrollbar = ttk.Scrollbar(self, orient=HORIZONTAL, command=self.OnHsb)
            self.xscrollbar.grid(row=3, column=1, sticky=E+W)

        self.canvas.configure(width=self.width,
            height=self.height-self.general['HEADERS ROWHEIGHT'], bg=self.background)
        self.canvas1.configure(width=self.width, height=self.general['HEADERS ROWHEIGHT'],
            background=self.general['HEADERS BACKCOLOR'],
            scrollregion=(0,0,self.totalwidth,10))
        self.canvas2.configure(height=self.height-self.general['HEADERS ROWHEIGHT'],
            background=self.general['HEADERS BACKCOLOR'])
        self.canvas3.configure(background=self.general['HEADERS BACKCOLOR'], height=self.general['HEADERS ROWHEIGHT'])
        if self.hscroll:
            self.canvas1.configure(xscrollcommand=self.xscrollbar.set)
            self.canvas.configure(xscrollcommand=self.xscrollbar.set)

        self.canvas.configure(cursor='watch')
        self.drawHeaders()
        self.drawData()
        self.canvas.configure(cursor='arrow')

    def drawHeaders(self):
        self.canvas1.delete(ALL)
        self.canvas3.delete(ALL)

        self.lb.configure(font=self.general['TABLE_TITLE FONT'], text=self.general['TABLE_TITLE'], wraplength=self.width)
        self.lb.grid(row=0, column=0, columnspan=2, pady=10, sticky=E+W)

        #vertical lines
        x = 2
        h = self.general['HEADERS ROWHEIGHT']
        for i in range(self.numOfCols-1):
            x += self.headers[i][1]
            self.canvas1.create_line(x, 0, x, h, width=2, fill='grey')

        #text
        y = self.general['HEADERS ROWHEIGHT'] / 2
        current_x=0
        for j in range(self.numOfCols):
            x = current_x+self.headers[j][1] / 2
            self.canvas1.create_text(x, y, width=self.headers[j][1]-6,
                font=self.general['HEADERS FONT'], anchor=CENTER, justify=CENTER,
                text=self.headers[j][0])
            current_x += self.headers[j][1]

    def redrawData(self):
        for i in range(self.visibleRows):
            for j in range(self.numOfCols):
                cellText = ''
                if self.dataCallback:
                    cellText = self.dataCallback(i + self.topRow, j)
                else:
                    cellText = self.grid_data[i + self.topRow][j]

                self.objectList[(i, j)][1].config(text=cellText)

            self.canvas2.itemconfig(self.rowObjects[i], text=str(i + 1 + self.topRow))

    def drawData(self):
        self.objectList = {}
        self.rowObjects = {}
        self.canvas.delete(ALL)
        self.canvas2.delete(ALL)
        y = 10

        if self.scrollAfter:
            self.canvas.after_cancel(self.scrollAfter)
            self.scrollAfter = None

        editfld = Entry(self.canvas, font=self.font, text='W')
        eid = self.canvas.create_window(20, 20, window=editfld)
        bb = self.canvas.bbox(eid)
        eheight = bb[3] - bb[1] + 2
        self.rowHeight = eheight
        charWidth = (bb[2] - bb[0]) // 20
        self.canvas.delete(eid)

        cwidth = self.canvas.winfo_height()
        scrH = 0
        if self.hscroll==True:
            self.rootWidget.update_idletasks()
            scrH = self.xscrollbar.winfo_height()
        self.visibleRows = (cwidth - scrH) // eheight
        if self.visibleRows > self.general['ROW COUNT'] - self.topRow:
            self.visibleRows = self.general['ROW COUNT'] - self.topRow

        for i in range(self.visibleRows):
            rowheight = eheight
            xtext = self.totalwidth + 30

            x = 2
            for j in range(self.numOfCols):
                xtext = x
                if not j:
                    xtext += 2
                ytext = y - 9
                rect = self.canvas.create_rectangle(xtext+5, y-9, xtext + 5 + self.headers[j][1]-7-10, y - 9 + rowheight - 1, outline=self.canvas['background'], tag='vgrid')

                cellText = ''
                if self.dataCallback:
                    cellText = self.dataCallback(i + self.topRow, j)
                else:
                    cellText = self.grid_data[i + self.topRow][j]

                t = None
                if self.headers[j][2] == 'left':
                    t = ttk.Label(self.canvas, text=cellText, anchor='w', background=self.canvas['background'])
                elif self.headers[j][2] == 'center':
                    t = ttk.Label(self.canvas, text=cellText, anchor='w', background=self.canvas['background'])
                else:
                    t = ttk.Label(self.canvas, text=cellText, anchor='e', background=self.canvas['background'])
                clip = self.canvas.create_window (xtext+6, y-8, height=rowheight-2, width=self.headers[j][1]-7, window=t, anchor='nw', tag='vgrid')
                self.objectList[(i,j)] = (rect, t)
                t.bind('<KeyPress>', self.onKeyPress)
                t.bind('<Control-Home>', self.topOfTable)
                t.bind('<Control-End>', self.endOfTable)
                t.bind('<ButtonPress-1>', self.onClick)
                t.bind('<Double-Button-1>', self.generateEditEvent)

                if platform.system() != 'Linux':
                    t.bind('<MouseWheel>', self.OnMouseWheel)
                    t.bind('<MouseWheel>', self.OnMouseWheel)
                else:
                    t.bind('<Button-4>', self.OnMouse4)
                    t.bind('<Button-5>', self.OnMouse5)
                    t.bind('<Button-4>', self.OnMouse4)
                    t.bind('<Button-5>', self.OnMouse5)

                x += self.headers[j][1]
            #horizontal lines
            y += eheight
            self.canvas.create_line(0, y-10, x, y-10, width=0, fill='grey')
            self.canvas2.create_line(0, y-10, 50, y-10, width=2, fill='grey')
            #row numbers
            self.rowObjects[i] = self.canvas2.create_text (24, y-rowheight/2-10,
                anchor=CENTER, text=str(i + 1 + self.topRow), font='Arial 10 bold')

        self.canvas.tag_bind('vgrid', '<KeyPress>', self.onKeyPress)
        self.canvas.tag_bind('vgrid', '<Control-Home>', self.topOfTable)
        self.canvas.tag_bind('vgrid', '<Control-End>', self.endOfTable)
        self.canvas.tag_bind('vgrid', '<ButtonPress-1>', self.onClick)
        self.canvas.tag_bind('vgrid', '<Double-Button-1>', self.generateEditEvent)
        self.canvas.bind('<KeyPress>', self.onKeyPress)
        self.canvas.bind('<Control-Home>', self.topOfTable)
        self.canvas.bind('<Control-End>', self.endOfTable)
        self.canvas.bind('<ButtonPress-1>', self.onClick)
        self.canvas.bind('<Double-Button-1>', self.generateEditEvent)

        if platform.system() != 'Linux':
            self.canvas2.bind('<MouseWheel>', self.OnMouseWheel)
            self.canvas.bind('<MouseWheel>', self.OnMouseWheel)
        else:
            self.canvas.bind('<Button-4>', self.OnMouse4)
            self.canvas.bind('<Button-5>', self.OnMouse5)
            self.canvas2.bind('<Button-4>', self.OnMouse4)
            self.canvas2.bind('<Button-5>', self.OnMouse5)
            self.canvas.tag_bind('vgrid', '<Button-4>', self.OnMouse4)
            self.canvas.tag_bind('vgrid', '<Button-5>', self.OnMouse5)

        #vertical lines
        x = 3
        for i in range(self.numOfCols-1):
            x += self.headers[i][1]
            self.canvas.create_line(x, 0, x, y-10, width=0, fill='grey', tag='li')
            self.canvas.tag_raise('li')
        #canvas2 vertical shadow
        h = y - 10
        if self.height > h:
            h = self.height

        self.vscroll = False
        if self.visibleRows < self.general['ROW COUNT']:
            self.yscrollbar = ttk.Scrollbar(self, orient=VERTICAL, command=self.OnVsb)
            self.yscrollbar.grid(row=2,column=2, sticky=N+S)
            self.vscroll = True
            self.canvas.config(scrollregion=(0,0,self.totalwidth,y-10))
            self.canvas2.config(scrollregion=(0,0,35,y-10))
        else:
            self.canvas.config(scrollregion=(0,0,self.totalwidth,
                self.height-self.general['HEADERS ROWHEIGHT']))
            self.canvas2.config(scrollregion=(0,0,35,
                self.height-self.general['HEADERS ROWHEIGHT']))

        self.canvas.yview_moveto(0)
        self.canvas.xview_moveto(0)
        self.canvas1.xview_moveto(0)
        self.canvas2.yview_moveto(0)

        if self.general['ROW COUNT'] > 0:
            self.hiliteRow(0)
        self.updateScrollBar()

    def setFocus(self):
        self.canvas.focus_set()
        if len(self.objectList):
            self.canvas.focus(self.objectList[(0, 0)][0])

    def OnHsb(self, *args):
        self.canvas.xview(*args)
        self.canvas1.xview(*args)

    def deferredRedraw(self):
        self.scrollAfter = None
        self.redrawData()

    def generateChangeEvent(self):
        curr = self.topRow + self.position
        if curr != self.lastEventRow:
            self.event_generate('<<RowChange>>', x=curr)

    def generateEditEvent(self, event=None):
        if self.position != -1 and self.general['ROW COUNT']:
            curr = self.topRow + self.position
            if curr != self.lastEventRow:
                self.event_generate('<<RowEnter>>', x=curr)

    def OnVsb(self, *args):
        if self.general['ROW COUNT'] <= 0:
            return

        if len(args) == 0:
            return

        if args[0] == 'scroll':
            if args[2] == 'units':
                if int(args[1]) > 0:
                    self.itemDown()
                elif int(args[1]) < 0:
                    self.itemUp()
            elif args[2] == 'pages':
                if int(args[1]) > 0:
                    self.pageDown()
                elif int(args[1]) < 0:
                    self.pageUp()

        if args[0] == 'moveto':
            # do not redraw immediately after user moves scrollbar
            # getting data may be very slow so dragging a scroll may
            # became sluggish, irregular or even almost impossible
            onesize = 1 / self.general['ROW COUNT']

            if float(args[1]) < 0 or float(args[1]) > 1.0:
                return

            cnt = int(float(args[1]) / onesize)

            if cnt >= self.topRow and cnt < self.topRow + self.visibleRows:
                self.hiliteRow(cnt - self.topRow)
            else:
                if cnt < self.topRow:
                    self.topRow = cnt
                    if self.scrollAfter:
                        self.canvas.after_cancel(self.scrollAfter)
                    self.scrollAfter = self.canvas.after(100, self.deferredRedraw)
                    self.hiliteRow(0)
                elif self.topRow < self.general['ROW COUNT'] - self.visibleRows:
                    self.topRow = cnt - self.visibleRows
                    if self.topRow > self.general['ROW COUNT'] - self.visibleRows:
                        self.topRow = self.general['ROW COUNT'] - self.visibleRows
                    if self.scrollAfter:
                        self.canvas.after_cancel(self.scrollAfter)
                    self.scrollAfter = self.canvas.after(100, self.deferredRedraw)
                    self.hiliteRow(self.visibleRows - 1)
            self.updateScrollBar()

    def OnMouse4(self, event):
        self.itemUp()
        return 'break'

    def OnMouse5(self, event):
        self.itemDown()
        return 'break'

    def OnMouseWheel(self, event):
        if event.delta > 0:
            self.itemUp()
        elif event.delta < 0:
            self.itemDown()
        return 'break'

    def hiliteRow(self, rowId, hilite=True):
        if rowId == self.position and hilite:
            self.generateChangeEvent()
            return

        for i in range(self.numOfCols):
            if self.position >= 0 and self.position < self.visibleRows:
                self.canvas.itemconfig(self.objectList[(self.position, i)][0], fill = self.canvas['background'])
                self.objectList[(self.position, i)][1].config(background = self.canvas['background'])

            if (hilite and rowId < self.visibleRows and rowId >= 0):
                self.canvas.itemconfig(self.objectList[(rowId, i)][0], fill = 'cyan')
                self.objectList[(rowId, i)][1].config(background = 'cyan')

        if not hilite:
            self.position = -1
        else:
            self.position = rowId
        self.generateChangeEvent()

    def onClick(self, event):
        mouse_x = event.x
        mouse_y = event.y

        if event.x_root < self.canvas.winfo_rootx() or event.y_root < self.canvas.winfo_rooty():
            return
        if event.x_root - self.canvas.winfo_rootx() >= self.canvas.winfo_width() or \
                event.y_root - self.canvas.winfo_rooty() >= self.canvas.winfo_height():
            return

        rowNo = (event.y_root - self.canvas.winfo_rooty() - 2) // self.rowHeight
        self.hiliteRow(rowNo)

        return

    def updateScrollBar(self):
        if not self.vscroll:
            return

        if self.general['ROW COUNT'] <= 0:
            self.yscrollbar.set(0, 1)

        onesize = 1 / self.general['ROW COUNT']
        startPos = (self.topRow + self.position) * onesize
        endPos   = startPos + onesize
        if endPos > 1:
            endPos = 1
        self.yscrollbar.set(startPos, endPos)
        self.generateChangeEvent()

    def topOfTable(self, event):
        if self.general['ROW COUNT'] <= 0:
            return
        if self.topRow != 0:
            self.topRow = 0
            self.redrawData()
        if self.position != 0:
            self.hiliteRow(0)
        self.updateScrollBar()

    def endOfTable(self, event):
        if self.general['ROW COUNT'] <= 0:
            return
        if self.general['ROW COUNT'] > self.visibleRows:
            topPosition = self.general['ROW COUNT'] - self.visibleRows
            if topPosition != self.topRow:
                self.topRow = topPosition
                self.redrawData()
        self.hiliteRow(self.visibleRows - 1)
        self.updateScrollBar()

    def itemDown(self):
        if self.position < self.visibleRows - 1:
            self.hiliteRow(self.position + 1)
        else:
            if self.topRow + self.visibleRows < self.general['ROW COUNT']:
                self.topRow += 1
                self.redrawData()
        self.updateScrollBar()

    def itemUp(self):
        if self.position > 0:
            self.hiliteRow(self.position - 1)
        else:
            if self.topRow > 0:
                self.topRow -= 1
                self.redrawData()
        self.updateScrollBar()

    def pageDown(self):
        if self.general['ROW COUNT'] <= 0:
            return

        if self.general['ROW COUNT'] <= self.visibleRows + self.topRow:
            self.hiliteRow(self.visibleRows - 1)
        else:
            topPosition = self.topRow + self.visibleRows
            if self.general['ROW COUNT'] - topPosition < self.visibleRows:
                topPosition = self.general['ROW COUNT'] - self.visibleRows
            if topPosition != self.topRow:
                self.topRow = topPosition
                self.redrawData()
        self.updateScrollBar()

    def pageUp(self):
        if self.general['ROW COUNT'] <= 0:
            return

        if self.topRow - self.visibleRows >= 0:
            self.topRow = self.topRow - self.visibleRows
            self.redrawData()
        elif self.topRow != 0:
            self.topRow = 0
            self.redrawData()
            self.hiliteRow(0)
        elif self.position != 0:
            self.hiliteRow(0)
        self.updateScrollBar()

    def onKeyPress(self, event):
        if event.keysym == 'Return' or event.keysym == 'Enter':
            self.generateEditEvent()
        if event.keysym == 'Home':
            self.canvas.xview_moveto(0)
            self.canvas1.xview_moveto(0)
        elif event.keysym == 'End':
            self.canvas.xview_moveto(1.0)
            self.canvas1.xview_moveto(1.0)
        elif event.keysym == 'Next':
            self.pageDown()
        elif event.keysym == 'Prior':
            self.pageUp()
        elif event.keysym == 'Down':
            self.itemDown()
        elif event.keysym == 'Up':
            self.itemUp()
        elif event.keysym == 'Right':
            self.canvas.xview('scroll', 1, 'units')
            self.canvas1.xview('scroll', 1, 'units')
        elif event.keysym == 'Left':
            self.canvas.xview('scroll', -1, 'units')
            self.canvas1.xview('scroll', -1, 'units')

    def getNumberOfRows(self):
        return self.general['ROW COUNT']

    def clearGrid(self):
        if self.scrollAfter:
            self.canvas.after_cancel(self.scrollAfter)
            self.scrollAfter = None

        self.canvas.delete(ALL)
        self.canvas2.delete(ALL)
        self.lb.configure(text='')
        self.canvas.yview_moveto(0)
        self.canvas.xview_moveto(0)
        self.canvas1.xview_moveto(0)
        self.canvas2.yview_moveto(0)
        self.grid_data = []
        self.vscroll = False
        try:
            self.yscrollbar.grid_forget()
        except AttributeError:
            pass
        self.position = 0
        self.topRow = 0

    def getSelectedRow(self):
        if self.position == -1 or not self.general['ROW COUNT']:
            return -1

        return self.position + self.topRow

    def ensureVisible(self, rowId):
        if rowId < 0 or rowId > self.general['ROW COUNT']:
            return

        if rowId >= self.topRow and rowId < self.topRow + self.visibleRows:
            self.hiliteRow(rowId - self.topRow)
        else:
            if rowId >= self.general['ROW COUNT'] - self.visibleRows:
                self.topRow = self.general['ROW COUNT'] - self.visibleRows
                self.hiliteRow(rowId - self.topRow)
            else:
                self.topRow = rowId
                self.hiliteRow(0)
            self.redrawData()

